// dependencies
var express = require('express');
var bodyParser = require('body-parser');   
var session = require('express-session');
var authentication= require('sequelize-authentication');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var passportsession= require('passport-session');
var app = express();
var cors = require('cors');
app.use(cors());
app.listen(3000);
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
var cookieParser = require('cookie-parser');
app.use(cookieParser());
var jsonParser = bodyParser.json();
app.use(session({
        secret: 'seppoonbi',   
        resave: true,
        saveUninitialized: true,
        cookie: { 
                maxAge: 24 * 60 * 1000
        }
}));
var request = require('request');

const { Pool } = require('pg')
const pool = new Pool({
        user: 'admin',
        host: 'localhost',
        database: 'test',
        password: 'ilovetea',
        port: 5432,
})


// passport conf
passport.use('local-login', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
},
        (username, password, done) => {
                console.log("Login process: username:", username, "password:", password);
                return pool.query('SELECT id, username, password_hash FROM "user" WHERE username=$1 AND password_hash=$2', [username, password])
                        .then((result)=> {
                                console.log('query done');
                                return done(null, result.rows[0]);
                        })
                        .catch((err) => {
                                console.log("/login: " + err);
                                return done(null, false, {message:'Wrong username or password'});
                        });
        }));

passport.serializeUser((user, done)=>{
        console.log("serialize ", user);
        done(null, user.id);
});

passport.deserializeUser((id, done)=>{
        console.log("deserialize ", id);
        pool.query('SELECT id, username FROM "user" WHERE id = $1', [id])
                .then((user)=>{
                        console.log("deserializeUser ", user);
                        return done(null, user);
                })
                .catch((err)=>{
                        return done(new Error(`User with the id ${id} does not exist`));
                })
});

app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json());

// Ensure authentication
app.use(function(req, res, next) {
        if (req.user == null && req.path.indexOf('/success') === 0)
        {
                res.redirect('/login');
        }
        if (req.user == null && req.path.indexOf('/student') === 0)
        {
                res.redirect('/login');
        }
        if (req.user == null && req.path.indexOf('/teacher') === 0)
        {
                res.redirect('/login');
        }
        next();
});

app.get('/', function (req, res) {
        res.redirect('/student');
})

app.get('/signup', function (req, res) {
        res.sendfile('/srv/eternal-kangaroo/html/signup.html');
})

// app itself
app.get('/login', function (req, res) {
        res.sendFile('/srv/eternal-kangaroo/html/login.html');
})

app.get('/error', function (req, res) {
        res.sendFile('/srv/eternal-kangaroo/html/error.html');
})

app.get('/success', function (req, res) {
        res.sendFile('/srv/eternal-kangaroo/html/success.html');
})

app.get('/student', jsonParser, function(req, res) {
        res.sendFile('/srv/eternal-kangaroo/html/student.html');
})

app.post('/api/login', passport.authenticate('local-login', {
        successRedirect : '/student', // redirect to the secure profile section
        failureRedirect : '/error', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
}));

app.get('/api/inQueue', jsonParser, function (req, res) {
        let responseString = pgJson('SELECT user_id, username, queued_at FROM user_of_room WHERE queued_at IS NOT null ORDER BY queued_at ASC');
        responseString.then(function(result) {
                res.json(result);
        });
        async function pgJson(query){
                var query = await pool.query(query);
                return query.rows;
        }
})

app.post('/api/nameFromId', function (req, res) {
        let responseString = pgJson('SELECT username FROM "user" WHERE id = $1', req.body.id);
        responseString.then(function(result) {
                res.send(result);
        })

        async function pgJson(query, user_id){
                var query = await pool.query(query, [user_id]);
                return(query.rows[0].username);
        }
})

app.post('/api/goQueue/', jsonParser, function (req, res) {
        console.log("GOQUEUE");
        let userIdQuery = getUserId();
        userIdQuery.then(function(result) {
                putToQueue(result);
                res.end();
        })

        async function putToQueue(userId){
                console.log("DAUSERID:",userId);
                pgQuery('UPDATE  user_of_room SET queued_at = now() WHERE user_id = $1', userId);
        }

        async function getUserId(){
                var userId = await req.session.passport.user
                return userId;
        }

        async function pgQuery(query, userId){
                var query = await pool.query(query, [userId]);
        }
})

app.post('/api/leaveQueue/', jsonParser, function (req, res) {
        console.log("LEAVEQUEUE");
        let userIdQuery = getUserId();
        userIdQuery.then(function(result) {
                putToQueue(result);
                res.end();
        })

        async function putToQueue(userId){
                console.log("DAUSERID:",userId);
                pgQuery('UPDATE user_of_room SET queued_at = null WHERE user_id = $1', userId);
        }

        async function getUserId(){
                var userId = await req.session.passport.user
                return userId;
        }

        async function pgQuery(query, userId){
                var query = await pool.query(query, [userId]);
        }
})

app.get('/api/next/', jsonParser, function (req, res) {
        console.log('NEEXT');
        pgQuery('UPDATE user_of_room SET queued_at = null WHERE queued_at = (SELECT queued_at FROM user_of_room ORDER BY queued_at LIMIT 1)');
        async function pgQuery(query){
                var query = await pool.query(query);
                res.end();
        }
})

app.get('/teacher/', function (req, res) {
        res.sendFile('/srv/eternal-kangaroo/html/teacher.html');
})

app.post('/api/signup', function (req, res) {
        if (req.body.ts == "Teacher") {
                var query = pgQuery("INSERT INTO \"user\" (username,password_hash,state) VALUES('$1','$2',1)",req.body.username,req.body.password);
                var query2 = pgQuery2("INSERT INTO user_of_room (user_id,username) SELECT id,username FROM \"user\"" )
                var query3 = pgQuery2("UPDATE user_of_room set room_id = 1 where room_id is null;")
                var query4 = pgQuery2("DELETE FROM user_of_room a USING (SELECT MIN(ctid) as ctid, user_id FROM user_of_room GROUP BY user_id HAVING COUNT(*) > 1) b WHERE a.user_id = b.user_id AND a.ctid <> b.ctid")
        }

        else {
                var query = pgQuery("INSERT INTO \"user\" (username, password_hash, state) VALUES('$1','$2',0)", req.body.username,req.body.password);
                var query2 = pgQuery2("INSERT INTO user_of_room (user_id,username) SELECT id,username FROM \"user\"" )
                var query3 = pgQuery2("UPDATE user_of_room set room_id = 1 where room_id is null")
                var query4 = pgQuery2("DELETE FROM user_of_room a USING (SELECT MIN(ctid) as ctid, user_id FROM user_of_room GROUP BY user_id HAVING COUNT(*) > 1) b WHERE a.user_id = b.user_id AND a.ctid <> b.ctid")
        }

        async function pgQuery(query, username,password){
                var query = await pool.query(query,[username,password]);
                res.end();
        }

        async function pgQuery2(query){
                var query = await pool.query(query);
                res.end();
        }
})
console.log("SUCCESSFULLY STARTED THE KANGAROO");
